import { useState, useRef, useEffect } from "react"

export default function StoneShaASCII() {
  const [isShown, setIsShown] = useState(false)

  const nameRef = useRef(null)

  useEffect(() => {
    if (!nameRef.current) return

    if (isShown) {
      nameRef.current.style.visibility = 'visible'
    }
    else if (!isShown)
      nameRef.current.style.visibility = 'hidden'
  }, [isShown])

  return (
    <>
      <div className="flex flex-row justify-center items-center w-full">
        <p
          className="stone-sha-ascii text-[1.25vw] w-min"
          onMouseEnter={() => setIsShown(true)}
          onMouseLeave={() => setIsShown(false)}
          onScroll={() => setIsShown(false)}
        >
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_&nbsp;&nbsp;&nbsp;&nbsp;_
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;/\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/\&nbsp;\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/\&nbsp;\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/\&nbsp;\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_&nbsp;&nbsp;&nbsp;&nbsp;/\&nbsp;\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;/\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;/\&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;/\&nbsp;/&nbsp;/\
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;/&nbsp;&nbsp;\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\_\&nbsp;\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;\&nbsp;\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;\&nbsp;\&nbsp;&nbsp;&nbsp;/\_\&nbsp;/&nbsp;&nbsp;\&nbsp;\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;/&nbsp;&nbsp;\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;/&nbsp;/&nbsp;&nbsp;&nbsp;/&nbsp;/&nbsp;//&nbsp;/&nbsp;&nbsp;\
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;/&nbsp;/\&nbsp;\__&nbsp;&nbsp;&nbsp;/\__&nbsp;\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;/\&nbsp;\&nbsp;\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;/\&nbsp;\&nbsp;\_/&nbsp;/&nbsp;//&nbsp;/\&nbsp;\&nbsp;\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;/&nbsp;/\&nbsp;\__&nbsp;&nbsp;&nbsp;/&nbsp;/_/&nbsp;&nbsp;&nbsp;/&nbsp;/&nbsp;//&nbsp;/&nbsp;/\&nbsp;\
          &nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;/&nbsp;/\&nbsp;\___\&nbsp;/&nbsp;/_&nbsp;\&nbsp;\&nbsp;&nbsp;&nbsp;/&nbsp;/&nbsp;/\&nbsp;\&nbsp;\&nbsp;&nbsp;&nbsp;/&nbsp;/&nbsp;/\&nbsp;\___/&nbsp;//&nbsp;/&nbsp;/\&nbsp;\_\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;/&nbsp;/\&nbsp;\___\&nbsp;/&nbsp;/\&nbsp;\__/&nbsp;/&nbsp;//&nbsp;/&nbsp;/\&nbsp;\&nbsp;\
          &nbsp;&nbsp;&nbsp;&nbsp;\&nbsp;\&nbsp;\&nbsp;\/___//&nbsp;/&nbsp;/\&nbsp;\&nbsp;\&nbsp;/&nbsp;/&nbsp;/&nbsp;&nbsp;\&nbsp;\_\&nbsp;/&nbsp;/&nbsp;/&nbsp;&nbsp;\/____//&nbsp;/_/_&nbsp;\/_/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\&nbsp;\&nbsp;\&nbsp;\/___//&nbsp;/\&nbsp;\___\/&nbsp;//&nbsp;/&nbsp;/&nbsp;&nbsp;\&nbsp;\&nbsp;\
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\&nbsp;\&nbsp;\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;/&nbsp;/&nbsp;&nbsp;\/_//&nbsp;/&nbsp;/&nbsp;&nbsp;&nbsp;/&nbsp;/&nbsp;//&nbsp;/&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;/&nbsp;//&nbsp;/____/\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\&nbsp;\&nbsp;\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;/&nbsp;/\/___/&nbsp;//&nbsp;/&nbsp;/___/&nbsp;/\&nbsp;\
          &nbsp;_&nbsp;&nbsp;&nbsp;&nbsp;\&nbsp;\&nbsp;\&nbsp;&nbsp;&nbsp;/&nbsp;/&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;/&nbsp;/&nbsp;&nbsp;&nbsp;/&nbsp;/&nbsp;//&nbsp;/&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;/&nbsp;//&nbsp;/\____\/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_&nbsp;&nbsp;&nbsp;&nbsp;\&nbsp;\&nbsp;\&nbsp;&nbsp;&nbsp;/&nbsp;/&nbsp;/&nbsp;&nbsp;&nbsp;/&nbsp;/&nbsp;//&nbsp;/&nbsp;/_____/&nbsp;/\&nbsp;\
          /_/\__/&nbsp;/&nbsp;/&nbsp;&nbsp;/&nbsp;/&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;/&nbsp;/___/&nbsp;/&nbsp;//&nbsp;/&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;/&nbsp;//&nbsp;/&nbsp;/______&nbsp;&nbsp;&nbsp;&nbsp;/_/\__/&nbsp;/&nbsp;/&nbsp;&nbsp;/&nbsp;/&nbsp;/&nbsp;&nbsp;&nbsp;/&nbsp;/&nbsp;//&nbsp;/_________/\&nbsp;\&nbsp;\
          \&nbsp;\/___/&nbsp;/&nbsp;&nbsp;/_/&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;/&nbsp;/____\/&nbsp;//&nbsp;/&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;/&nbsp;//&nbsp;/&nbsp;/_______\&nbsp;&nbsp;&nbsp;\&nbsp;\/___/&nbsp;/&nbsp;&nbsp;/&nbsp;/&nbsp;/&nbsp;&nbsp;&nbsp;/&nbsp;/&nbsp;//&nbsp;/&nbsp;/_&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;__\&nbsp;\_\
          &nbsp;\_____\/&nbsp;&nbsp;&nbsp;\_\/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\/_________/&nbsp;\/_/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\/_/&nbsp;\/__________/&nbsp;&nbsp;&nbsp;&nbsp;\_____\/&nbsp;&nbsp;&nbsp;\/_/&nbsp;&nbsp;&nbsp;&nbsp;\/_/&nbsp;\_\___\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/____/_/
        </p>
        <div onMouseEnter={() => setIsShown(true)} ref={nameRef} className="p-1 md:p-7 bg-white rounded-full border-black border-2 absolute z-10">
          <i className="text-ascii-blue text-lg md:text-4xl font-normal">Stone Sha</i>
        </div>
      </div>
    </>
  )
}