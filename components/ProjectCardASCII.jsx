import { useState, useRef, useEffect } from "react"

export default function ProjectCardASCII({ children, title, description }) {
  const [isShown, setIsShown] = useState(false)

  const asciiRef = useRef(null)
  const titleRef = useRef(null)

  useEffect(() => {
    if (!asciiRef.current) return

    if (isShown) {
      asciiRef.current.style.display = 'block'
      titleRef.current.style.display = 'none'
      titleRef.current.animate(
        {
          opacity: [1, 0],
        },
        250
      )
      asciiRef.current.animate(
        {
          opacity: [0, 1],
        },
        250
      )
    }
    else if (!isShown) {
      asciiRef.current.style.display = 'none'
      titleRef.current.style.display = 'block'
      titleRef.current.animate(
        {
          opacity: [0, 1],
        },
        250
      )
      asciiRef.current.animate(
        {
          opacity: [1, 0],
        },
        250
      )
    }

  }, [isShown])

  return (
    <>
      <div className="my-2">
        <div
          className="md:h-24 w-full flex flex-col justify-end"
          onMouseEnter={() => setIsShown(true)}
          onMouseLeave={() => setIsShown(false)}
          onScroll={() => setIsShown(false)}
        >
          <div className="project-card-title" ref={titleRef}>
            {title}
          </div>
          <div className="project-card-ascii" ref={asciiRef}>
            {children}
          </div>
        </div>
        <hr className="w-full border-black border-t-2 mt-2" />
        <div>
          {description}
        </div>
      </div>
    </>
  )
}