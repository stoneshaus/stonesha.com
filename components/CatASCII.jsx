/* eslint-disable react/no-unescaped-entities */

export default function CatASCII() {

  return (
    <div className="w-[212px] h-[105px] font-bold tracking-tighter text-ascii-blue leading-tight text-base fixed bottom-0 right-0">

      &nbsp;&nbsp;|\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_,,,--,,_&nbsp;&nbsp;,)<br />
      &nbsp;&nbsp;/,`.-'`'&nbsp;&nbsp;&nbsp;-,&nbsp;&nbsp;;-;;'<br />
      &nbsp;|,4-&nbsp;&nbsp;)&nbsp;)-,_&nbsp;)&nbsp;/\<br />
      '---''(_/--'&nbsp;(_/-'<br />
    </div>
  )
}