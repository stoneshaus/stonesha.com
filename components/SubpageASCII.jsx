import { useState, useRef, useEffect } from "react"

export default function SubpageASCII({ title, children }) {
  const [isShown, setIsShown] = useState(false)

  const nameRef = useRef(null)

  useEffect(() => {
    if (!nameRef.current) return

    if (isShown) {
      nameRef.current.style.visibility = 'visible'
    }
    else if (!isShown)
      nameRef.current.style.visibility = 'hidden'
  }, [isShown])

  return (
    <>
      <div className="flex flex-row justify-start items-center w-full">
        <p
          className="subpage-ascii justify-self-start w-min text-[2.5vw] sm:text-[1vw] md:text-[0.75vw] font-bold mb-4"
          onMouseEnter={() => setIsShown(true)}
          onMouseLeave={() => setIsShown(false)}
          onScroll={() => setIsShown(false)}
        >
          {children}
        </p>
        <div onMouseEnter={() => setIsShown(true)} ref={nameRef} className="absolute p-0.5 md:p-2 bg-white rounded-full border-black border-2">
          <i className="text-ascii-blue text-xl font-normal">{title}</i>
        </div>
      </div>
    </>
  )
}