import ReactFullpage from "@fullpage/react-fullpage"
import StoneShaASCII from "../components/StoneShaASCII"
import SubpageASCII from "../components/SubpageASCII"
import ProjectCardASCII from "../components/ProjectCardASCII"
import CatASCII from "../components/CatASCII"

export default function Home() {

  return (
    <div className="mx-8 md:mx-12 my-10">

      <h1 className="text-ascii-blue font-bold -mb-1.5">Stone Sha</h1>
      <nav className="block md:flex flex-row justify-start">
        <hr className="w-full my-auto border-black border-t-2" />
        <div className="w-full md:w-3/5 lg:w-2/5 flex flex-row justify-around">
          <button className="text-black text-lg font-normal">About</button>
          <button className="text-black text-lg font-normal">Projects</button>
          <button className="text-black text-lg font-normal">Contact</button>
        </div>
      </nav>

      <CatASCII />

      <ReactFullpage
        navigation
        scrollingSpeed={1000}
        render={({ state, fullpageApi }) =>
          <ReactFullpage.Wrapper>
            <div className="section">
              <div className="justify-start">
                <br />
                <i className="ml-10 font-medium">Hello! I&apos;m</i>
                <StoneShaASCII />
                <hr className="border-black border-t-2 mt-7 md:mt-14" />
                <p className="w-68 sm:w-80 bottom-52 md:bottom-24 absolute mb-1">
                  I&apos;m a developer, guitar player, and video game enthusiast.
                  I use this website personally to keep organized about the things
                  I&apos;m passionate about.
                </p>
              </div>
            </div>
            <div className="section">
              <SubpageASCII title='Projects'>
                &nbsp;____&nbsp;&nbsp;____&nbsp;&nbsp;&nbsp;___&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_&nbsp;_____&nbsp;____&nbsp;_____&nbsp;____
                |&nbsp;&nbsp;_&nbsp;\|&nbsp;&nbsp;_&nbsp;\&nbsp;/&nbsp;_&nbsp;\&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;|&nbsp;____/&nbsp;___|_&nbsp;&nbsp;&nbsp;_/&nbsp;___|
                |&nbsp;|_&#41;&nbsp;|&nbsp;|_&#41;&nbsp;|&nbsp;|&nbsp;|&nbsp;|_&nbsp;&nbsp;|&nbsp;|&nbsp;&nbsp;_||&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;|&nbsp;\___&nbsp;\
                |&nbsp;&nbsp;__/|&nbsp;&nbsp;_&nbsp;&gt;|&nbsp;|_|&nbsp;|&nbsp;|_|&nbsp;|&nbsp;|__|&nbsp;|___&nbsp;&nbsp;|&nbsp;|&nbsp;&nbsp;___&#41;&nbsp;|
                |_|&nbsp;&nbsp;&nbsp;|_|&nbsp;\_\\___/&nbsp;\___/|_____\____|&nbsp;|_|&nbsp;|____/
              </SubpageASCII>
              <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 sm:gap-x-36 sm:gap-y-20">
                <ProjectCardASCII
                  title="cat-chat"
                  description="Online Chat Client written with React. Backend written with Elixir"
                >
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/\_____/\
                  &nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;o&nbsp;&nbsp;&nbsp;o&nbsp;&nbsp;\
                  &nbsp;&nbsp;&nbsp;(&nbsp;==&nbsp;&nbsp;^&nbsp;&nbsp;==&nbsp;)
                  &nbsp;&nbsp;(&nbsp;(&nbsp;&nbsp;)&nbsp;&nbsp;&nbsp;(&nbsp;&nbsp;)&nbsp;)
                  &nbsp;(__(__)___(__)__)
                </ProjectCardASCII>
                <ProjectCardASCII
                  title="notez"
                  description="A note keeping application written with React and Firebase."
                >
                  &nbsp;&nbsp;__________________<br />
                  &nbsp;/\________________/`-.<br />
                  &lt;()&gt;______________&lt;&nbsp;&nbsp;&nbsp;&nbsp;##<br />
                  &nbsp;\/________________\,-
                </ProjectCardASCII>
                <ProjectCardASCII
                  title="utils"
                  description="General purpose utilities written with SvelteKit."
                >
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_&nbsp;&nbsp;&nbsp;(_)&nbsp;_&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;_&nbsp;&nbsp;&nbsp;_&nbsp;|&nbsp;|_&nbsp;&nbsp;_&nbsp;|&nbsp;|&nbsp;___&nbsp;
                  |&nbsp;|&nbsp;|&nbsp;||&nbsp;__||&nbsp;||&nbsp;|/&nbsp;__|
                  |&nbsp;|_|&nbsp;||&nbsp;|_&nbsp;|&nbsp;||&nbsp;|\__&nbsp;\
                  &nbsp;\__,_|&nbsp;\__||_||_||___/
                </ProjectCardASCII>
              </div>
            </div>
            <div className="section">
              <SubpageASCII title='Contact'>
                &nbsp;&nbsp;____&nbsp;___&nbsp;&nbsp;_&nbsp;&nbsp;&nbsp;_&nbsp;_____&nbsp;&nbsp;_&nbsp;&nbsp;&nbsp;&nbsp;____&nbsp;_____
                &nbsp;/&nbsp;___/&nbsp;_&nbsp;\|&nbsp;\&nbsp;|&nbsp;|_&nbsp;&nbsp;&nbsp;_|/&nbsp;\&nbsp;&nbsp;/&nbsp;___|_&nbsp;&nbsp;&nbsp;_|
                |&nbsp;|&nbsp;&nbsp;|&nbsp;|&nbsp;|&nbsp;|&nbsp;&nbsp;\|&nbsp;|&nbsp;|&nbsp;|&nbsp;/&nbsp;_&nbsp;\|&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;|
                |&nbsp;|__|&nbsp;|_|&nbsp;|&nbsp;|\&nbsp;&nbsp;|&nbsp;|&nbsp;|/&nbsp;___&nbsp;\&nbsp;|___&nbsp;&nbsp;|&nbsp;|
                &nbsp;\____\___/|_|&nbsp;\_|&nbsp;|_/_/&nbsp;&nbsp;&nbsp;\_\____|&nbsp;|_|
              </SubpageASCII>
            </div>
          </ReactFullpage.Wrapper>
        }
      />
    </div>
  )
}